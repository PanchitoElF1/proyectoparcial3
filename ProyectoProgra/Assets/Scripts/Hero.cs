﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
   
    private float velocidad;
    private float fuerzaSalto;
    private float fuerzaY;
    private int points;
    private string tipo;

    public Hero()
    {
        Tipo = "Normal Hero";
        Velocidad= 10;
        fuerzaSalto=800;
        points = 1;
        fuerzaY = 15;
    }

   


    public float Velocidad
    {
        get { return velocidad; }
        set { velocidad = value; }
    }

    public string Tipo
    {
        get { return tipo; }
        set { tipo = value; }
    }

    public float FuerzaSalto
    {
        get { return fuerzaSalto; }
        set { fuerzaSalto = value; }
    }

    public int Points
    {
        get { return points; }
        set { points = value; }
    }

    public float FuerzaY
    {
        get { return fuerzaY; }
        set { fuerzaY = value; }
    }

}
