﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aleatorio : MonoBehaviour
{
    public GameObject[] obj;//lista de objetos que van a spawnear
    public float tiempoMin;//tiempo minimo de aleatorio
    public float tiempoMax;//tiempo maxim de aleatorio

    void Start()
    {
        Generar();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Generar()
    {                              //escoge en 0 y el numero que le asignes
        Instantiate(obj[Random.Range(0, obj.Length)], transform.position, Quaternion.identity);//va instanciar un obj de la lista de objetos en esa posicion
        Invoke("Generar", Random.Range(tiempoMin, tiempoMax));//llama a la misma funcion y escoge en cuanto tiempo va spawnear el objeto
    }
}
