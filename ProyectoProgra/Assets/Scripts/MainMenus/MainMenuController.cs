﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuController : MonoBehaviour
{
    public GameObject controlsPanel;
    public GameObject menuPanel;

    public void Jugar(string _gameScene)
    {
        SceneManager.LoadScene(_gameScene);
    }

    public void Salir()
    {
        Application.Quit();
        Debug.Log("Afuera");
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }
}
