﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CondicionDerrota : MonoBehaviour
{
    public GameObject PanelDerrota;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Pinchos"))
        {
            Destroy(gameObject);
            PanelDerrota.SetActive(true);
            Time.timeScale = 0;
        }
    }

   

}
