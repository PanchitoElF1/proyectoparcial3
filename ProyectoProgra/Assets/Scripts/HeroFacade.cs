﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroFacade : Hero
{
    

    public void JumpBetter()
    {    
        FuerzaSalto = 1300;
    }

    public void MoveFast()
    {
        
        Velocidad = 40;
        
    }

   public void Puntos()
    {
        Points = 1;
    }

    public void FuerzaEnY()
    {
        FuerzaY = 15;
    }
}
