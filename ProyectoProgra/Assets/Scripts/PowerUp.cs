﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PowerUp : MonoBehaviour
{
    HeroFacade puntos;
  

    void Start()
    {
       
        puntos = new HeroFacade();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
           
            Destroy(gameObject);
            Score.instance.scoreValue += puntos.Points; //se va a sumar un punto por cada vez que se tome una llave
            Score.instance.score.text = "Llaves: " + Score.instance.scoreValue; //se le coloca al texto en pantalla


        }


    }

    
}
