﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultFacade : MonoBehaviour
{
   
    private Rigidbody2D rb;

    private HeroFacade Hero1;
    public bool active;
    public bool active2;


    [Header("Salto")]
    public Transform groundCheck;
    public bool jump = false;
    public bool grounded = false;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        active = false;
        Hero1 = new HeroFacade();
       
    }

    public void Cambio() //powerup aumento de velocidad
    {
        if (active)
        {
            active = true;
            Hero1.MoveFast();
            
        }
        else
        {
            active = true;
            Hero1.MoveFast();
          
        }
       
    }

    public void Cambio2() //powerup aumento de fuerza en salto
    {
        if (active2)
        {
            active2 = true;
            Hero1.JumpBetter();

        }
        else
        {
            active2 = true;
            Hero1.JumpBetter();

        }

    }


    void Update()
    {
        Ground();
      
    }

    void FixedUpdate()
    {
        Move();
        Jump();
    }

    public void Move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");     //movimiento en eje x                                              
        Vector2 movement = new Vector2(moveHorizontal * Hero1.Velocidad, rb.velocity.y);       //velocidad de movimiento                
        rb.velocity = movement;
    }

    void Ground()
    {

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        if (grounded)
        {


        }
        if (Input.GetButtonDown("Jump") && grounded)
        {

            jump = true;
        }
        if (grounded == false)
        {
            rb.velocity += Vector2.up * Physics.gravity.y * 8 * Time.deltaTime;
        }
    }

    void Jump()
    {
        if (jump)
        {
            rb.AddForce(new Vector2(0f, Hero1.FuerzaSalto));
            jump = false;

        }
    }

    
}
