﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public GameObject panelCambio;
    public GameObject panelCambio2;

    public int scoreValue = 0;

    public Text score;


    public static Score instance;

    void Start()
    {
        score = GetComponent<Text>();

        if (instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    private void Update()
    {
        aparecer();
    }

    void aparecer()//funcion para activar panel de boost
    {
        if (scoreValue >= 2)
        {
            panelCambio.SetActive(true);
        }

        if (scoreValue >= 3)
        {
            panelCambio2.SetActive(true);
        }
    }
}
