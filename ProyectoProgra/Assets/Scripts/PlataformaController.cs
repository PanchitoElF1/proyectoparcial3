﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaController : MonoBehaviour
{
    HeroFacade AgregarF;
    Rigidbody2D rb;
    

    void Start()
    {
        AgregarF = new HeroFacade();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.up * AgregarF.FuerzaY;//Se agrega una fuerza a la izquierda en Y
    }
}
